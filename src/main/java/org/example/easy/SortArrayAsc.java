package org.example.easy;

public class SortArrayAsc {
public static void main(String[] args) {
        int[] arr = {5, 2, 8, 7, 1};
        sortArrayAsc(arr);
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }

    public static void sortArrayAsc(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

}
