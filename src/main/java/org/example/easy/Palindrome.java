package org.example.easy;

public class Palindrome {
    public static void main(String[] args) {
        System.out.println(isPalindrome(23532));
        System.out.println(isPalindromeString(23532));
        System.out.println(isPalindromeHalfReverse(23532));
    }

    // Approach 1
    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        int original = x;
        int reversed = 0;
        while (x != 0) {
            int digit = x % 10;
            x /= 10;
            reversed = reversed * 10 + digit;
        }
        return original == reversed;
    }

    // Approach 2  - Using String
    public static boolean isPalindromeString(int x) {
        return String.valueOf(x).equals(new StringBuilder(String.valueOf(x)).reverse().toString());
    }

    // approach 3 half reverse
    public static boolean isPalindromeHalfReverse(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }
        int reversed = 0;
        while (x > reversed) {
            int digit = x % 10;
            x /= 10;
            reversed = reversed * 10 + digit;
        }
        return x == reversed || x == reversed / 10;
    }


}
