package org.example.easy;

/**
 * Có 2 con kangaroo cùng nhảy theo chiều dương trên 1 trục toạ độ x.
 * Giả định cả 2 nhảy được số bước giống nhau trong các khoảng thời gian bằng nhau,
 * xuất phát tại cùng 1 thời điểm từ vị trí lần lượt là x1 và x2, mỗi lần nhảy xa tương ứng v1 và v2 đơn vị.
 * Hãy viết đoạn mã giả để kiểm tra xem có tồn tại thời điểm nào 2 chú kangaroo gặp nhau ở cùng 1 vị trí x hay không.
 */
public class KangaruMeetSameTime {

    public static void main(String[] args) {
        System.out.println(kangarooMeetSameTime(0, 3, 4, 5));
        System.out.println(kangarooMeetSameTime(6, 4, 5, 3));
        System.out.println(kangarooMeetSameTime(0, 4, 5, 3));
        System.out.println(kangarooMeetSameTime(2, 4, 8, 3));
    }

    public static boolean kangarooMeetSameTime(int x1, int v1, int x2, int v2) {
        // Nếu một con kangaroo có vị trí ban đầu gần hơn mà lại chỉ có tốc độ nhảy chậm hơn hoặc bằng thì chúng sẽ không bao giờ gặp nhau
        if ((x1 > x2 && v1 >= v2) || (x2 > x1 && v2 >= v1)) {
            return false;
        }
        return true;
    }

}
