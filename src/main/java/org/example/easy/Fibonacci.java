package org.example.easy;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(fibonacciRecursion(6));
    }

    // fibonacci n using recursion
    public static int fibonacciRecursion(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacciRecursion(n - 1) + fibonacciRecursion(n - 2);
    }
}
