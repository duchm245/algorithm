package org.example.easy;

import java.util.Arrays;

/**
 * Cho 1 chuỗi chỉ bao gồm các ký tự la tinh viết thường (a-z).
 * Hãy viết đoạn mã giả mô tả thuật toán tìm ra một chuỗi khác bằng cách thay đổi vị trí các ký tự trên chuỗi cũ
 * sao cho chuỗi mới lớn hơn chuỗi cũ theo thứ tự từ điển.
 * Trong trường hợp có nhiều kết quả, thì chỉ chọn chuỗi nhỏ nhất trong các kết quả có thể.
 */
public class RearrangeToGreaterStringOrigin {
    public static void main(String[] args) {
        String str = "acdb";
        System.out.println("Chuỗi ban đầu: " + str);
        System.out.println("Chuỗi lớn nhất từ chuỗi ban đầu: " + rearrange(str));

        str = "cdba";
        System.out.println("Chuỗi ban đầu: " + str);
        System.out.println("Chuỗi lớn nhất từ chuỗi ban đầu: " + rearrange(str));
    }

    public static String rearrange(String str) {
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        int pivotIndex = findPivot(charArray, length);

        if (pivotIndex == -1) {
            return "Không có chuỗi lớn hơn.";
        }

        swap(charArray, pivotIndex, pivotIndex + 1);
        Arrays.sort(charArray, pivotIndex + 1, length);

        return new String(charArray);
    }
    private static int findPivot(char[] arr, int length) {
        // Tìm vị trí của ký tự đầu tiên mà có thể tăng được
        for (int i = length - 2; i >= 0; i--) {
            if (arr[i] < arr[i + 1]) {
                return i;
            }
        }
        return -1;
    }

    private static void swap(char[] arr, int i, int j) {
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
